#include <iostream>
#include <string>
#include <stack>
#include "math.h"


class Application {
public:
    bool isValidBrackets(std::string userInput) {
        std::stack<char> stk;
        for (int i = 0; i < userInput.size(); i++) {
            if (stk.empty()) {
                stk.push(userInput[i]);
                continue;
            }
            char bracket;
            switch (stk.top()) {
                case '{':
                    bracket = '}';
                    break;
                case '[':
                    bracket = ']';
                    break;
                case '(':
                    bracket = ')';
                    break;
            }
            if (userInput[i] == bracket) stk.pop();
            else stk.push(userInput[i]);
        }
        if (stk.size() == 0) return true;
        else return false;
    }

    int run() {
        std::string userInput;
        std::cin >> userInput;
        if (isValidBrackets(userInput)) {
            std::cout << "Valid!" << std::endl;
        } else {
            std::cout << "Invalid!" << std::endl;
        }
        std::cout << sqrt(25) << std::endl;
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
