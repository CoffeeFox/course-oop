#pragma once
#include <iostream>

class iPrintable {
    virtual void print() = 0;
};