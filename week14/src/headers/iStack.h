#pragma once

class iStack {
    virtual int pop() = 0;
    virtual int push(int element) = 0;
    virtual int top() = 0;
    virtual bool isEmpty() = 0;
};