#pragma once
#include <iostream>
#include "iStack.h"
#include "iPrintable.h"

class Stack : iStack, iPrintable{
private:
    int arr[10];
    int topElement;
public:
    Stack();
    bool isEmpty();
    bool isFull();
    int push(int element);
    int pop();
    int top();
    void print();
};