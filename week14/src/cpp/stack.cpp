#include "stack.h"

Stack::Stack() {
    topElement = -1;
    } // Initialize top to -1 to indicate an empty stack

bool Stack::isEmpty() {
    return (topElement == -1);}

bool Stack::isFull() {
    return (topElement == 10 - 1);}

int Stack::push(int element) {
    if (!isFull()) {
        topElement++;
        arr[topElement] = element;
        return 0;
    } else {
        return 1;
    }
}

int Stack::pop() {
    if (!isEmpty()) {
        int poppedElement = arr[topElement];
        topElement--;
        return 0;
    } else {
        return 1;
    }
}

int Stack::top() {
    if (!isEmpty()) {
        return arr[topElement];
    } else {
        return -1;
    }
}

void Stack::print() {
    std::cout << this->top() << std::endl;
    return;
}