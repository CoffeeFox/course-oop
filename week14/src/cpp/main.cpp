#include "stack.h"

int main() {
    Stack st;
    st.push(11);
    st.push(678);
    st.push(525);
    st.print();
    st.pop();
    st.pop();
    st.print();
}