#include <iostream>
#include <string>
#include <limits>


class Application {
public:
    void printInType(std::string userInput, std::string requestedType) {
        try {
            if (requestedType == "int") {
                std::cout << std::stoi(userInput) << std::endl;
                return;
            } else if (requestedType == "bool") {
                int value = std::stoul(userInput);
                if (value) {
                    std::cout << "True" << std::endl;
                } else {
                    std::cout << "False" << std::endl;
                }
                return;
            } else if (requestedType == "double") {
                std::cout << std::stod(userInput) << std::endl;
                return;
            } else if (requestedType == "long") {
                std::cout << std::stoul(userInput) << std::endl;
                return;
            } else if (requestedType == "short") {
                int value = std::stoi(userInput);
                if ((value > std::numeric_limits<short>::max()) or 
                    (value < std::numeric_limits<short>::min())) {
                    throw std::out_of_range("error");
                } else {
                    std::cout << (short)value << std::endl;
                }
                return;
            } else if (requestedType == "float") {
                std::cout << std::stof(userInput) << std::endl;
                return;
            } else {
                std::cout << "ERROR: Unknown type" << std::endl;
                return;
            }
        } catch (std::invalid_argument e) {
            std::cout << "ERROR: Unable to convert such argument" << std::endl;
            return;
        } catch (std::out_of_range e) {
            std::cout << "ERROR: Out of range" << std::endl;
            return;
        }
    }        

    int run() {
        std::cout << "Enter requested type:" << std::endl;
        std::string requestedType;
        std::cin >> requestedType;
        std::cin.clear();

        std::cout << "Enter your data:" << std::endl;
        std::string userInput;
        std::cin >> userInput;

        printInType(userInput, requestedType);
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
