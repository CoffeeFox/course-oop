#include <iostream>
#include <fstream>


class Application {
public:
    void printFile(std::string filename) {
        std::ifstream file;
        std::string fileText;
        try {
            file.open(filename);
            if (!file.is_open()) {
                throw 1;
            }
        } catch(int e) {
            std::cout << "The file doesn't exist!" << std::endl;
            return;
        }
        std::cout << "Reading the file:" << std::endl;
        while (getline(file, fileText)) {
            std::cout << fileText;
        }
        std::cout << std::endl;
        file.close();
        return;
    }

    int run() {
        std::string filename;
        std::cout << "Enter the filename:" << std::endl;
        std::cin >> filename;
        printFile(filename);
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
