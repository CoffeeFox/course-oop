#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <algorithm>


typedef std::map<std::string, std::vector<std::string>> DICTIONARY;


void printDictionary(DICTIONARY m) {
    for (auto it = m.begin(); it != m.end(); ++it) {
        std::cout << it->first << " - ";
        for (int i = 0; i < it->second.size(); i++) {
            std::cout << it->second[i];
            if (i + 1 < it->second.size()) {
                std::cout << ", ";
            }
        }
        std::cout << std::endl;
    }
    return;
}


class Application {
public:
    DICTIONARY getLatEng(DICTIONARY engLat) {
        DICTIONARY latEng;
        // Get all unique translations
        for (auto it = engLat.begin(); it != engLat.end(); ++it) {
            for (int i = 0; i < it->second.size(); i++) {
                std::vector<std::string> temp = {};
                latEng[it->second[i]] = temp;
            }
        }
        // Get all matching words for unique translations
        for (auto itLat = latEng.begin(); itLat != latEng.end(); ++itLat) {
            for (auto itEng = engLat.begin(); itEng != engLat.end(); ++itEng) {
                if (std::find(itEng->second.begin(), itEng->second.end(), itLat->first) != itEng->second.end()) {
                    itLat->second.push_back(itEng->first);
                }
            }
        }

        return latEng;
    }

    DICTIONARY getEngLat() {
        DICTIONARY engLat;
        std::string userInput;
        int amount;
        std::cout << "Enter the amount of words to process:" << std::endl;
        std::cin >> amount;
        std::cin.clear();

        std::string word;
        int pos;
        std::cin.ignore();
        for (int i = 0; i < amount; i++) {
            std::cout << "Enter next definition:" << std::endl;
            std::getline(std::cin, userInput);

            pos = userInput.find(" - ");
            word = userInput.substr(0, pos);
            userInput.erase(0, pos + 3);
            std::vector<std::string> translations;
            while ((pos = userInput.find(", ")) != std::string::npos) {
                translations.push_back(userInput.substr(0, pos));
                userInput.erase(0, pos + 2);
            }
            translations.push_back(userInput.substr(0, userInput.back()));
            engLat[word] = translations;
        }
        return engLat;
    }

    int run() {
        DICTIONARY engLat = getEngLat();
        DICTIONARY latEng = getLatEng(engLat);
        std::cout << "\n\n";
        std::cout << latEng.size() << std::endl;
        printDictionary(latEng);
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
