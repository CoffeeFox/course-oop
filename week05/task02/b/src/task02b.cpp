#include <iostream>
#include <map>
#include <string>
#include <vector>


typedef std::map<std::string, int> ENTITIES;
typedef std::map<std::string, ENTITIES> SALES;


void printSales(SALES m) {
    for (auto it = m.begin(); it != m.end(); ++it) {
        std::cout << it->first << ":" << std::endl;
        for (auto itEnt = it->second.begin(); itEnt != it->second.end(); ++itEnt) {
            std::cout << itEnt->first << " " << itEnt->second << std::endl;
        }
    }
    return;
}


class Application {
public:
    SALES getSales() {
        SALES totalSales;
        std::string userInput;

        std::string sellerName;
        std::string entityName;
        int amount;

        while (std::getline(std::cin, userInput)) {
            if (userInput.size() == 0) break;
            int pos = userInput.find(" ");
            sellerName = userInput.substr(0, pos);
            userInput.erase(0, pos+1);
            pos = userInput.find(" ");
            entityName = userInput.substr(0, pos);
            userInput.erase(0, pos+1);
            amount = std::stoi(userInput.substr(0, userInput.back()));
            totalSales[sellerName][entityName] += amount;
        }
        return totalSales;
    }

    int run() {
        SALES totalSales = getSales();
        printSales(totalSales);
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
