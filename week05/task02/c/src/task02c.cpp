#include <iostream>
#include "fib.h"

std::unordered_map<int, int> umap = std::unordered_map<int, int>{{0, 0}, {1, 1}};

class Application {
public:  
    int run() {
        int userInput;
        std::cout << "Enter int (amount of calculations):" << std::endl;
        std::cin >> userInput;
        std::cout << fibMem(userInput) << std::endl;
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
