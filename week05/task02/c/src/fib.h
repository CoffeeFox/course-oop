#pragma once

#include <cmath>
#include <unordered_map>


int fib(int n) { 
    if (n <= 1) 
        return n; 
    return fib(n - 1) + fib(n - 2); 
} 

int fibMem(int n) {
    extern std::unordered_map<int, int> umap;
    if (umap.find(n) == umap.end()) {
        int val = fibMem(n-1) + fibMem(n-2);
        umap[n] = val;
        return val;
    }
    return umap[n];
}