#include <fstream>
#include <chrono>
#include "fib.h"

std::unordered_map<int, int> umap = std::unordered_map<int, int>{{0, 0}, {1, 1}};

int main() {
    std::ofstream file("results.txt");
        for (int n = 4; n < 40; n++) {
            for (int i = 0; i < 10; i++) {
                auto start = std::chrono::high_resolution_clock::now();
                fib(n);
                auto stop = std::chrono::high_resolution_clock::now();
                auto duration = std::chrono::duration_cast<std::chrono::microseconds>(stop-start);
                file << n << " | " << duration.count() << std::endl;
            }
        }
        file << "\n\n";
        for (int n = 4; n < 40; n++) {
            for (int i = 0; i < 10; i++) {
                auto start = std::chrono::high_resolution_clock::now();
                fibMem(n);
                auto stop = std::chrono::high_resolution_clock::now();
                umap = std::unordered_map<int, int>{{0, 0}, {1, 1}};
                auto duration = std::chrono::duration_cast<std::chrono::nanoseconds>(stop-start);
                file << n << " | " << duration.count() << std::endl;
            }
        }
        file.close();
    return 0;
}