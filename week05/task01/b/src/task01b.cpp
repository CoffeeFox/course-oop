#include <iostream>
#include <vector>


class Application {
public:
    int maxProfit(std::vector<int> array) {
        int max = 0;
        int profit;
        int minIndex = 0;
        for (int i = 0; i < array.size(); i++) {
            if (array[i] < array[minIndex]) minIndex = i;
        }
        for (int i = minIndex; i < array.size(); i++) {
            if (max < (array[i] - array[minIndex])) max = array[i] - array[minIndex];
        }
        return max;
    }

    void processStringCycle(std::string& userInput, std::vector<int>& array) {
        if (userInput.find(",") == std::string::npos) {
            array.push_back(std::stoi(userInput.substr(0, userInput.back())));
        } else {
            array.push_back(std::stoi(userInput.substr(0, userInput.find(",")+1)));
        }
        userInput.erase(0, userInput.find(",")+1);
        return;
    }

    int run() {
        std::string userInput;
        std::vector<int> array;
        std::cout << "Enter numbers: " << std::endl;
        std::cin >> userInput;  // Input 7,2,1 and not 7, 2, 1 | NO SPACES ALLOWED

        std::cout << "\n\n";
        while (userInput.find(",") != std::string::npos) {
            processStringCycle(userInput, array);
        }
        processStringCycle(userInput, array);

        std::cout << maxProfit(array) << std::endl;
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
