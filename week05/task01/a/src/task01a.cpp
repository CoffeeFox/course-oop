#include <iostream>
#include <vector>
#include <algorithm>


void displayVector(std::vector<int> array) {
    for (int i : array) {
        std::cout << i << " ";
    }
    std::cout << std::endl;
    return;
}


std::vector<std::vector<int>> perms(std::vector<int> array) {
    std::vector<std::vector<int>> result;
    sort(array.begin(), array.end());
    do {
    result.push_back(array);
    } while (next_permutation(array.begin(), array.end()));
    return result;
}


class Application {
public:

    void processStringCycle(std::string& userInput, std::vector<int>& array) {
        if (userInput.find(",") == std::string::npos) {
            array.push_back(std::stoi(userInput.substr(0, userInput.back())));
        } else {
            array.push_back(std::stoi(userInput.substr(0, userInput.find(",")+1)));
        }
        userInput.erase(0, userInput.find(",")+1);
        return;
    }

    int run() {
        std::string userInput;
        std::vector<int> array;
        std::cout << "Enter numbers: " << std::endl;
        std::cin >> userInput;  // Input 7,2,1 and not 7, 2, 1 | NO SPACES ALLOWED
        
        std::cout << "\n\n";
        while (userInput.find(",") != std::string::npos) {
            processStringCycle(userInput, array);
        }
        processStringCycle(userInput, array);

        std::vector<std::vector<int>> result = perms(array);
        for (int i = 0; i < result.size(); i++) {
            displayVector(result[i]);
        }
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
