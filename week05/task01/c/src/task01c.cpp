#include <iostream>
#include <vector>


void displayVector(std::vector<unsigned int> array) {
    for (int i : array) {
        std::cout << i << " ";
    }
    std::cout << std::endl;
    return;
}


int distance(int a, int b) {
    if (a > b) {
        return a - b;
    } else {
        return b - a;
    }
}


class Application {
public:
    void processStringCycle(std::string& userInput, std::vector<unsigned int>& array) {
        int temp;
        if (userInput.find(",") == std::string::npos) {
            temp = std::stoi(userInput.substr(0, userInput.back()));
            if (temp < 0) {temp = -temp;}
            array.push_back(temp);
        } else {
            temp = std::stoi(userInput.substr(0, userInput.find(",")+1));
            if (temp < 0) {temp = -temp;}
            array.push_back(temp);
        }
        userInput.erase(0, userInput.find(",")+1);
        return;
    }

    int maxVolume(std::vector<unsigned int> array) {
        int max = 0;
        int i1 = 0;
        int i2 = 1;
        if (array.size() == 2) {
            if (array[i1] < array[i2]) {
                if (max < (array[i1])) max = array[i1];
            } else {
                if (max < (array[i2])) max = array[i2];
            }
        }
        while ((i1 != array.size()-1) and (i2 != array.size())) {
            if (i2 != array.size()) i2++;
            else i1++;
            
            unsigned int distance = i2 - i1;
            if (array[i1] < array[i2]) {
                if (max < (array[i1] * distance)) {
                    max = array[i1] * distance;
                }
            } else {
                if (max < (array[i2] * distance)) {
                    max = array[i2] * distance;
                }
            }
        }
        return max;
    }

    int run() {
        std::string userInput;
        std::vector<unsigned int> array;
        std::cout << "Enter numbers: " << std::endl;
        std::cin >> userInput;  // Input 7,2,1 and not 7, 2, 1 | NO SPACES ALLOWED
        while (userInput.find(",") != std::string::npos) {
            processStringCycle(userInput, array);
        }
        processStringCycle(userInput, array);
        std::cout << maxVolume(array) << std::endl;
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
