#include <iostream>
#include <set>


class Application {
public:
     void processStringCycle(std::string& userInput, std::set<int>& result) {
        if (userInput.find(",") == std::string::npos) {
            result.insert(std::stoi(userInput.substr(0, userInput.back())));
        } else {
            result.insert(std::stoi(userInput.substr(0, userInput.find(",")+1)));
        }
        userInput.erase(0, userInput.find(",")+1);
        return;
    }

    int run() {
        std::string userInput;
        std::cin >> userInput;
        std::set<int> result;
        while (userInput.find(",") != std::string::npos) {
            processStringCycle(userInput, result);
        }
        processStringCycle(userInput, result);
        for (auto it = result.begin(); it != result.end(); it++) {
            std::cout << *it << std::endl;
        }
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
