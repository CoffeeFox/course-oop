#pragma once
#include <fstream>
#include <iostream>
#include <vector>
#include <tuple>
#include <string>
#include <nlohmann/json.hpp>
#include "state_controller.h"
#include "player.h"
#include "car.h"
#include "bike.h"
#include "dealership.h"


class Database {
public:
    int save(StateController& controller);
    StateController load(std::string filename);

    nlohmann::json carsToJSON(StateController& controller);
    nlohmann::json bikesToJSON(StateController& controller);
    nlohmann::json playersToJSON(StateController& controller);
    nlohmann::json carDealersToJSON(StateController& controller);
    nlohmann::json bikeDealersToJSON(StateController& controller);

    std::vector<Player> loadPlayers(std::string filename);
    std::vector<Dealership<Car>> loadCarDealers(std::string filename);
    std::vector<Dealership<Bike>> loadBikeDealers(std::string filename);
    std::vector<Car> loadCars(std::string filename);
    std::vector<Bike> loadBikes(std::string filename);

    nlohmann::json parseJson(std::string filename);
};