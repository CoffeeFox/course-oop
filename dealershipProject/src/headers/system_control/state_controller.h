#pragma once
#include "player.h"
#include "car.h"
#include "bike.h"
#include "dealership.h"


class StateController {
private:
    std::vector<Player> players;
    std::vector<Dealership<Car>> carDealerships;
    std::vector<Dealership<Bike>> bikeDealerships;
    std::vector<Car> cars;
    std::vector<Bike> bikes;

public:
    StateController() {};
    StateController(std::vector<Player> players,
    std::vector<Dealership<Car>> carDealerships,
    std::vector<Dealership<Bike>> bikeDealerships,
    std::vector<Car> cars,
    std::vector<Bike> bikes)
    : players(players), carDealerships(carDealerships),
    bikeDealerships(bikeDealerships), cars(cars), bikes(bikes) {};

    friend class UIMenu;
    friend class UIManager;
    friend class UISpawner;
    friend class UIRemover;
    friend class Database;
};

// These includes should be at the bottom
// because the declaration of these classes should follow
// the declaration of SystemController or else
// you will have a compilation error
// because there will be no UI declarations for
// friend statements to work
#include "menu.h"
#include "spawner.h"
#include "remover.h"
#include "manager.h"
#include "database.h"