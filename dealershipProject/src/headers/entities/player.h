#pragma once
#include <string>
#include <iostream>
#include <vector>
#include "car.h"
#include "bike.h"
#include "user.h"


class Player : User {
private:
    std::string name;
    std::vector<Car> cars;
    std::vector<Bike> bikes;
    std::vector<std::string> licenses;
    double money;
public:
    Player();

    Player(std::string userName);
    Player(std::string userName, std::vector<Car> cars,
        std::vector<Bike> bikes, double money,
        std::vector<std::string> licenses);

    std::string getName() const;
    double getMoney() const;
    std::vector<Car> getCars() const;
    std::vector<Bike> getBikes() const;
    std::vector<std::string> getLicenses() const;

    int setName(std::string newName);
    int setCars(std::vector<Car> newVehicles);
    int setBikes(std::vector<Bike> newVehicles);
    int setMoney(double newMoney);
    int setLicenses (std::vector<std::string> newLicenses);

    int addVehicle(Car car);
    int addVehicle(Bike bike);
    int removeVehicle(Car car);
    int removeVehicle(Bike bike);

    int addMoney(double m);
    int subtractMoney(double m);
    int addLicense(std::string license);
    int removeLicense(std::string license);

    friend std::ostream& operator<<(std::ostream& stream, const Player& pl);
};