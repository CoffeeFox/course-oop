#pragma once
#include <iostream>
#include <string>


class Vehicle {
private:
    std::string brand;
    std::string model;
    std::string ownerName;
    std::string plateNumber;
    int condition;
    std::string color;

    virtual std::ostream& print(std::ostream& stream) const;
public:
    Vehicle(); 
    Vehicle(std::string brand, std::string model, std::string ownerName,
    std::string plateNumber, int enteredCondition, std::string color);

    friend std::ostream& operator<<(std::ostream& stream, const Vehicle& vic);

    std::string getBrand() const;
    std::string getModel() const;
    std::string getOwnerName() const;
    std::string getPlateNumber() const;
    int getCondition() const;
    std::string getColor() const;

    int setBrand(std::string newBrand);
    int setModel(std::string newModel);
    int setOwnerName(std::string newOwnerName);
    int setPlateNumber(std::string newPlateNumber);
    int setCondition(int newCondition);
    int setColor(std::string newColor);
    
    friend bool operator==(Vehicle v1, Vehicle v2);
};
