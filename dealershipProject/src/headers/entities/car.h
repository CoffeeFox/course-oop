#pragma once
#include <iostream>
#include <string>
#include "vehicle.h"


class Car: public Vehicle {
private:
    std::string drive;
public: 
    Car();
    Car(std::string brand, std::string model, std::string ownerName,
    std::string plateNumber, int enteredCondition, 
    std::string color, std::string drive);

    std::string getDrive() const;
    int setDrive(std::string newDrive);
    std::ostream& print(std::ostream& stream) const;
    friend bool operator==(Car v1, Car v2);
};