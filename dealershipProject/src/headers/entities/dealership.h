#pragma once
#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include "player.h"


template<typename T>
class Dealership {
private:
    std::string name;
    std::vector<std::tuple<T, double>> catalog;
public:
    Dealership();
    Dealership(std::string enteredName);
    Dealership(std::string name, std::vector<std::tuple<T, double>>);

    std::string getName() const;
    std::vector<std::tuple<T, double>> getCatalog() const;
    int setName(std::string enteredName);
    int setCatalog(std::vector<std::tuple<T, double>> enteredVehicles);
    int sell(T vehicle, Player& player);
    
    bool isPlayerAllowed(Player& player);
};