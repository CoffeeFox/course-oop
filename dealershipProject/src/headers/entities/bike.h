#pragma once
#include <iostream>
#include <string>
#include "vehicle.h"


class Bike: public Vehicle {
private:
    std::string type;
public: 
    Bike();
    Bike(std::string brand, std::string model, std::string ownerName,
    std::string plateNumber, int enteredCondition, 
    std::string color, std::string type);

    std::string getType() const;
    int setType(std::string newType);
    std::ostream& print(std::ostream& stream) const;
    friend bool operator==(Bike v1, Bike v2);
};