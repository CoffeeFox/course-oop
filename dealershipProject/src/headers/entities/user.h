#pragma once
#include <string>


class User {
public:
    virtual std::string getName() const = 0;
    virtual int setName(std::string newName) = 0;
    virtual double getMoney() const = 0;
    virtual int setMoney(double newMoney) = 0;
};