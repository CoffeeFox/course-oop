#pragma once
#include "state_controller.h"
#include "ui_module.h"


class UIRemover : UIModule {
public:
    void showMenu(StateController& controller);
    void removeCar(StateController& controller);
    void removeBike(StateController& controller);
    void removeDealership(StateController& controller);
    void removePlayer(StateController& controller);
};