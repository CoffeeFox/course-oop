#pragma once
#include "state_controller.h"
#include "ui_module.h"


class UISpawner : UIModule {
public:
    void showMenu(StateController& controller);
    void createCar(StateController& controller);
    void createBike(StateController& controller);
    void createDealership(StateController& controller);
    void createPlayer(StateController& controller);
};