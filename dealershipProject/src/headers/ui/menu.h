#pragma once
#include "state_controller.h"
#include "ui_module.h"


class UIMenu : UIModule {
public:
    void showMenu(StateController& controller);
    void callSpawner(StateController& controller);
    void callManager(StateController& controller);
    void callRemover(StateController& controller);
    void quit();
};