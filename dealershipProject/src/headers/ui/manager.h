#pragma once
#include "state_controller.h"
#include "ui_module.h"


class UIManager : UIModule {
public:
    void showMenu(StateController& controller);
    void assignCarToDealer(StateController& controller);
    void assignBikeToDealer(StateController& controller);
    void manageLicenses(StateController& controller);
    void purchaseVehicle(StateController& controller);
};