#pragma once
#include "state_controller.h"


class UIModule {
public:
    virtual void showMenu(StateController& controller) = 0;
};