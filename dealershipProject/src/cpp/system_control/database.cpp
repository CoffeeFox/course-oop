#include <fstream>
#include <iostream>
#include <vector>
#include <tuple>
#include <string>
#include <nlohmann/json.hpp>
#include "database.h"
#include "state_controller.h"


int Database::save(StateController& controller) {
    nlohmann::json finalValue = nlohmann::json();
    finalValue["players"] = playersToJSON(controller);
    finalValue["cars"] = carsToJSON(controller);
    finalValue["bikes"] = bikesToJSON(controller);
    finalValue["carDealers"] = carDealersToJSON(controller);
    finalValue["bikeDealers"] = bikeDealersToJSON(controller);

    std::ofstream file("data.json");
    file << nlohmann::to_string(finalValue);
    file.close();
    return 0;
}

nlohmann::json Database::carsToJSON(StateController& controller) {
    nlohmann::json cars = nlohmann::json::array();
    for (int j = 0; j < controller.cars.size(); j++) {
        Car tempCar = controller.cars[j];
        nlohmann::json carJson;
        carJson["brand"] = tempCar.getBrand();
        carJson["model"] = tempCar.getModel();
        carJson["ownerName"] = tempCar.getOwnerName();
        carJson["plateNumber"] = tempCar.getPlateNumber();
        carJson["condition"] = tempCar.getCondition();
        carJson["color"] = tempCar.getColor();
        carJson["drive"] = tempCar.getDrive();
        cars.push_back(carJson);
    }
    return cars;
}

nlohmann::json Database::bikesToJSON(StateController& controller) {
    nlohmann::json bikes = nlohmann::json::array();
    for (int j = 0; j < controller.bikes.size(); j++) {
        Bike tempBike = controller.bikes[j];
        nlohmann::json bikeJson;
        bikeJson["brand"] = tempBike.getBrand();
        bikeJson["model"] = tempBike.getModel();
        bikeJson["ownerName"] = tempBike.getOwnerName();
        bikeJson["plateNumber"] = tempBike.getPlateNumber();
        bikeJson["condition"] = tempBike.getCondition();
        bikeJson["color"] = tempBike.getColor();
        bikeJson["type"] = tempBike.getType();
        bikes.push_back(bikeJson);
    }
    return bikes;
}

nlohmann::json Database::playersToJSON(StateController& controller) {
    nlohmann::json playersJSON = nlohmann::json::array();
    for (int i = 0; i < controller.players.size(); i++) {
        nlohmann::json tempValue;
        nlohmann::json tempArrayCars = nlohmann::json::array();
        nlohmann::json tempArrayBikes = nlohmann::json::array();
        nlohmann::json tempArrayLicenses = nlohmann::json::array();
        tempValue["name"] = controller.players[i].getName();
        tempValue["money"] = controller.players[i].getMoney();

        std::string license;
        for (int j = 0; j < controller.players[i].getLicenses().size(); j++) {
            license = controller.players[i].getLicenses()[j];
            tempArrayLicenses.push_back(license);
        }
        tempValue["licenses"] = tempArrayLicenses;

        for (int j = 0; j < controller.players[i].getCars().size(); j++) {
            Car tempCar = controller.players[i].getCars()[j];
            nlohmann::json carJson;
            carJson["brand"] = tempCar.getBrand();
            carJson["model"] = tempCar.getModel();
            carJson["ownerName"] = tempCar.getOwnerName();
            carJson["plateNumber"] = tempCar.getPlateNumber();
            carJson["condition"] = tempCar.getCondition();
            carJson["color"] = tempCar.getColor();
            carJson["drive"] = tempCar.getDrive();
            tempArrayCars.push_back(carJson);
        }
        tempValue["cars"] = tempArrayCars;

        for (int j = 0; j < controller.players[i].getBikes().size(); j++) {
            Bike tempBike = controller.players[i].getBikes()[j];
            nlohmann::json bikeJson;
            bikeJson["brand"] = tempBike.getBrand();
            bikeJson["model"] = tempBike.getModel();
            bikeJson["ownerName"] = tempBike.getOwnerName();
            bikeJson["plateNumber"] = tempBike.getPlateNumber();
            bikeJson["condition"] = tempBike.getCondition();
            bikeJson["color"] = tempBike.getColor();
            bikeJson["type"] = tempBike.getType();
            tempArrayBikes.push_back(bikeJson);
        }
        tempValue["bikes"] = tempArrayBikes;

        playersJSON.push_back(tempValue);

        tempValue.clear();
        tempArrayCars = nlohmann::json::array();
        tempArrayBikes = nlohmann::json::array();
        tempArrayLicenses = nlohmann::json::array();
    }
    return playersJSON;
}

nlohmann::json Database::carDealersToJSON(StateController& controller) {
    nlohmann::json catalog = nlohmann::json::array();
    nlohmann::json carDealersJson = nlohmann::json::array();
    nlohmann::json carDealer;
    for (int i = 0; i < controller.carDealerships.size(); i++) {
        for (int j = 0; j < controller.carDealerships[i].getCatalog().size(); j++) {
            Car tempCar = std::get<0>(controller.carDealerships[i].getCatalog()[j]);
            nlohmann::json carJson = nlohmann::json();
            carJson["brand"] = tempCar.getBrand();
            carJson["model"] = tempCar.getModel();
            carJson["ownerName"] = tempCar.getOwnerName();
            carJson["plateNumber"] = tempCar.getPlateNumber();
            carJson["condition"] = tempCar.getCondition();
            carJson["color"] = tempCar.getColor();
            carJson["drive"] = tempCar.getDrive();
            carJson["price"] = std::get<1>(controller.carDealerships[i].getCatalog()[j]);
            catalog.push_back(carJson);
        }
        carDealer["catalog"] = catalog;
        carDealer["name"] = controller.carDealerships[i].getName();
        carDealersJson.push_back(carDealer);
        carDealer = nlohmann::json();
    }
    return carDealersJson;
}

nlohmann::json Database::bikeDealersToJSON(StateController& controller) {
    nlohmann::json catalog = nlohmann::json::array();
    nlohmann::json bikeDealersJson = nlohmann::json::array();
    nlohmann::json bikeDealer;
    for (int i = 0; i < controller.bikeDealerships.size(); i++) {
        for (int j = 0; j < controller.bikeDealerships[i].getCatalog().size(); j++) {
            Bike tempBike = std::get<0>(controller.bikeDealerships[i].getCatalog()[j]);
            nlohmann::json bikeJson = nlohmann::json();
            bikeJson["brand"] = tempBike.getBrand();
            bikeJson["model"] = tempBike.getModel();
            bikeJson["ownerName"] = tempBike.getOwnerName();
            bikeJson["plateNumber"] = tempBike.getPlateNumber();
            bikeJson["condition"] = tempBike.getCondition();
            bikeJson["color"] = tempBike.getColor();
            bikeJson["type"] = tempBike.getType();
            bikeJson["price"] = std::get<1>(controller.bikeDealerships[i].getCatalog()[j]);
            catalog.push_back(bikeJson);
        }
        bikeDealer["catalog"] = catalog;
        bikeDealer["name"] = controller.bikeDealerships[i].getName();
        bikeDealersJson.push_back(bikeDealer);
        bikeDealer = nlohmann::json();
    }
    return bikeDealersJson;
}

StateController Database::load(std::string filename) {
    return StateController(loadPlayers(filename), loadCarDealers(filename),
    loadBikeDealers(filename), loadCars(filename), loadBikes(filename));
}

std::vector<Player> Database::loadPlayers(std::string filename) {
    std::vector<Player> players;
    nlohmann::json parsed = parseJson(filename);
    for (int i = 0; i < parsed["players"].size(); i++) {
        std::vector<Car> tempArrayCars;
        std::vector<Bike> tempArrayBikes;
        std::vector<std::string> tempArrayLicenses;
        for (int j = 0; j < parsed["players"][i]["cars"].size(); j++) {
            Car tempVehicle = 
            Car(parsed["players"][i]["cars"][j]["brand"].template get<std::string>(),
                    parsed["players"][i]["cars"][j]["model"].template get<std::string>(),
                    parsed["players"][i]["cars"][j]["ownerName"].template get<std::string>(),
                    parsed["players"][i]["cars"][j]["plateNumber"].template get<std::string>(),
                    parsed["players"][i]["cars"][j]["condition"].template get<int>(),
                    parsed["players"][i]["cars"][j]["color"].template get<std::string>(),
                    parsed["players"][i]["cars"][j]["drive"].template get<std::string>());
            tempArrayCars.push_back(tempVehicle);
        }
        for (int j = 0; j < parsed["players"][i]["bikes"].size(); j++) {
            Bike tempVehicle = 
            Bike(parsed["players"][i]["bikes"][j]["brand"].template get<std::string>(),
                    parsed["players"][i]["bikes"][j]["model"].template get<std::string>(),
                    parsed["players"][i]["bikes"][j]["ownerName"].template get<std::string>(),
                    parsed["players"][i]["bikes"][j]["plateNumber"].template get<std::string>(),
                    parsed["players"][i]["bikes"][j]["condition"].template get<int>(),
                    parsed["players"][i]["bikes"][j]["color"].template get<std::string>(),
                    parsed["players"][i]["bikes"][j]["type"].template get<std::string>());
            tempArrayBikes.push_back(tempVehicle);
        }
        for (int j = 0; j < parsed["players"][i]["licenses"].size(); j++) {
            std::string license = parsed["players"][i]["licenses"][j];
            tempArrayLicenses.push_back(license);
        }
        players.push_back(Player(parsed["players"][i]["name"], 
            tempArrayCars, tempArrayBikes, parsed["players"][i]["money"].template get<double>(), tempArrayLicenses));
    }
    return players;
}

std::vector<Dealership<Car>> Database::loadCarDealers(std::string filename) {
    std::vector<Dealership<Car>> carDealers;
    nlohmann::json parsed = parseJson(filename);
    for (int i = 0; i < parsed["carDealers"].size(); i++) {
        std::vector<std::tuple<Car, double>> catalog;
        Dealership<Car> tempDealer;

        for (int j = 0; j < parsed["carDealers"][i]["catalog"].size(); j++) {
            Car tempVehicle = 
            Car(parsed["carDealers"][i]["catalog"][j]["brand"].template get<std::string>(),
                    parsed["carDealers"][i]["catalog"][j]["model"].template get<std::string>(),
                    parsed["carDealers"][i]["catalog"][j]["ownerName"].template get<std::string>(),
                    parsed["carDealers"][i]["catalog"][j]["plateNumber"].template get<std::string>(),
                    parsed["carDealers"][i]["catalog"][j]["condition"].template get<int>(),
                    parsed["carDealers"][i]["catalog"][j]["color"].template get<std::string>(),
                    parsed["carDealers"][i]["catalog"][j]["drive"].template get<std::string>());
            catalog.push_back(std::tuple<Car, double>(tempVehicle, parsed["carDealers"][i]["catalog"][j]["price"].template get<double>()));
        }

        tempDealer.setCatalog(catalog);
        tempDealer.setName(parsed["carDealers"][i]["name"].template get<std::string>());
        carDealers.push_back(tempDealer);
    }
    return carDealers; 
}

std::vector<Dealership<Bike>> Database::loadBikeDealers(std::string filename) {
    std::vector<Dealership<Bike>> bikeDealers;
    nlohmann::json parsed = parseJson(filename);
    for (int i = 0; i < parsed["bikeDealers"].size(); i++) {
        std::vector<std::tuple<Bike, double>> catalog;
        Dealership<Bike> tempDealer;

        for (int j = 0; j < parsed["bikeDealers"][i]["catalog"].size(); j++) {
            Bike tempVehicle = 
            Bike(parsed["bikeDealers"][i]["catalog"][j]["brand"].template get<std::string>(),
                    parsed["bikeDealers"][i]["catalog"][j]["model"].template get<std::string>(),
                    parsed["bikeDealers"][i]["catalog"][j]["ownerName"].template get<std::string>(),
                    parsed["bikeDealers"][i]["catalog"][j]["plateNumber"].template get<std::string>(),
                    parsed["bikeDealers"][i]["catalog"][j]["condition"].template get<int>(),
                    parsed["bikeDealers"][i]["catalog"][j]["color"].template get<std::string>(),
                    parsed["bikeDealers"][i]["catalog"][j]["type"].template get<std::string>());
            catalog.push_back(std::tuple<Bike, double>(tempVehicle, parsed["bikeDealers"][i]["catalog"][j]["price"].template get<double>()));
        }

        tempDealer.setCatalog(catalog);
        tempDealer.setName(parsed["bikeDealers"][i]["name"].template get<std::string>());
        bikeDealers.push_back(tempDealer);
    }
    return bikeDealers; 
}

std::vector<Car> Database::loadCars(std::string filename) {
    nlohmann::json parsed = parseJson(filename);
    std::vector<Car> cars;
    for (int i = 0; i < parsed["cars"].size(); i++) {
        Car tempVehicle = 
        Car(parsed["cars"][i]["brand"].template get<std::string>(),
                parsed["cars"][i]["model"].template get<std::string>(),
                parsed["cars"][i]["ownerName"].template get<std::string>(),
                parsed["cars"][i]["plateNumber"].template get<std::string>(),
                parsed["cars"][i]["condition"].template get<int>(),
                parsed["cars"][i]["color"].template get<std::string>(),
                parsed["cars"][i]["drive"].template get<std::string>());
        cars.push_back(tempVehicle);
    }
    return cars;
}

std::vector<Bike> Database::loadBikes(std::string filename) {
    nlohmann::json parsed = parseJson(filename);
    std::vector<Bike> bikes;
    for (int i = 0; i < parsed["bikes"].size(); i++) {
        Bike tempVehicle = 
        Bike(parsed["bikes"][i]["brand"].template get<std::string>(),
                parsed["bikes"][i]["model"].template get<std::string>(),
                parsed["bikes"][i]["ownerName"].template get<std::string>(),
                parsed["bikes"][i]["plateNumber"].template get<std::string>(),
                parsed["bikes"][i]["condition"].template get<int>(),
                parsed["bikes"][i]["color"].template get<std::string>(),
                parsed["bikes"][i]["type"].template get<std::string>());
        bikes.push_back(tempVehicle);
    }
    return bikes;
}

nlohmann::json Database::parseJson(std::string filename) {
    std::ifstream file(filename);
    nlohmann::json parsed;
    try {parsed = nlohmann::json::parse(file);} 
    catch (nlohmann::json_abi_v3_11_3::detail::parse_error) {return nlohmann::json();}
    file.close();
    return parsed;
}