#include <iostream>
#include "remover.h"
#include "state_controller.h"


void UIRemover::showMenu(StateController& controller) {
    while (true) {
        system("clear");
        std::cout << "1 - Remove player\n" <<
        "2 - Remove car\n" << "3 - Remove bike\n" <<
        "4 - Remove dealership\n" <<
        "0 - Return to menu" << std::endl;

        int response;
        std::cin >> response;
        std::cin.clear();

        switch(response) {
            case 1:
                this->removePlayer(controller);
                break;
            case 2:
                this->removeCar(controller);
                break;
            case 3:
                this->removeBike(controller);
                break;
            case 4:
                this->removeDealership(controller);
                break;
            case 0:
                return;
        }
    }
    return;
}

void UIRemover::removePlayer(StateController& controller) {
    std::cout << "What player to remove?:" << std::endl;
    for (int i = 0; i < controller.players.size(); i++) {
        std::cout << i + 1 << " - " <<
        controller.players[i] << std::endl;
    }
    int userInput;
    std::cin >> userInput;
    std::cin.clear();

    controller.players.erase(controller.players.begin() + userInput - 1);
    return;
}

void UIRemover::removeCar(StateController& controller) {
    std::cout << "What car to remove?:" << std::endl;
    for (int i = 0; i < controller.cars.size(); i++) {
        std::cout << i + 1 << " - " <<
        controller.cars[i] << std::endl;
    }
    int userInput;
    std::cin >> userInput;
    std::cin.clear();

    controller.cars.erase(controller.cars.begin() + userInput - 1);
    return;
    return;
}

void UIRemover::removeBike(StateController& controller) {
    std::cout << "What bike to remove?:" << std::endl;
    for (int i = 0; i < controller.bikes.size(); i++) {
        std::cout << i + 1 << " - " <<
        controller.bikes[i] << std::endl;
    }
    int userInput;
    std::cin >> userInput;
    std::cin.clear();

    controller.bikes.erase(controller.bikes.begin() + userInput - 1);
    return;
}

void UIRemover::removeDealership(StateController& controller) {
    std::cout << "0 - Bike Dealerships, 1 - Car Dealerships" << std::endl;
    int userInput;
    std::cin >> userInput;
    std::cin.clear();
    if (userInput) {
        std::cout << "What dealership to remove?:" << std::endl;
        for (int i = 0; i < controller.carDealerships.size(); i++) {
            std::cout << i + 1 << " - " <<
            controller.carDealerships[i].getName() << std::endl;
        }
        userInput;
        std::cin >> userInput;
        std::cin.clear();

        controller.carDealerships.erase(controller.carDealerships.begin() + userInput - 1);
    } else {
        std::cout << "What dealership to remove?:" << std::endl;
        for (int i = 0; i < controller.bikeDealerships.size(); i++) {
            std::cout << i + 1 << " - " <<
            controller.bikeDealerships[i].getName() << std::endl;
        }
        userInput;
        std::cin >> userInput;
        std::cin.clear();

        controller.bikeDealerships.erase(controller.bikeDealerships.begin() + userInput - 1);
    }
    return;
}