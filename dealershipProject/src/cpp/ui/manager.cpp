#include <iostream>
#include "manager.h"
#include "state_controller.h"
#include "player.h"
#include "bike.h"
#include "car.h"
#include "dealership.h"


void UIManager::showMenu(StateController& controller) {
    while (true) {
        system("clear");
        std::cout << "1 - Assign car to dealer\n" <<
        "2 - Assign bike to dealer\n" <<
        "3 - Manage licenses\n" <<
        "4 - Purchase vehicle\n" <<
        "0 - Return to menu" << std::endl;

        int response;
        std::cin >> response;
        std::cin.clear();

        switch(response) {
            case 1:
                this->assignCarToDealer(controller);
                break;
            case 2:
                this->assignBikeToDealer(controller);
                break;
            case 3:
                this->manageLicenses(controller);
                break;
            case 4:
                this->purchaseVehicle(controller);
                break;
            case 0:
                return;
        }
    }
    return;
}

void UIManager::assignCarToDealer(StateController& controller) {
    int userInput;
    int carId;
    int dealershipId;
    double price = 150.67;

    std::cout << "What car to assign?: " << std::endl;
    for (int i = 0; i < controller.cars.size(); i++) {
        std::cout << i + 1 << " - " <<
        controller.cars[i] << std::endl;
    }
    std::cin >> carId;
    std::cin.clear();
    carId--;

    std::cout << "Which dealership?: " << std::endl;
    for (int i = 0; i < controller.carDealerships.size(); i++) {
        std::cout << i + 1 << " - " <<
        controller.carDealerships[i].getName() << std::endl;
    }
    std::cin >> dealershipId;
    std::cin.clear();
    dealershipId--;

    std::cout << "For what price? (example: 150.67): " << std::endl;
    std::cin >> price;
    std::cin.clear();

    std::vector<std::tuple<Car, double>> catalog = controller.carDealerships[dealershipId].getCatalog();
    catalog.push_back(std::tuple<Car, double>{controller.cars[carId], price});
    controller.carDealerships[dealershipId].setCatalog(catalog);
    controller.cars.erase(controller.cars.begin() + carId);
    return;
}

void UIManager::assignBikeToDealer(StateController& controller) {
    int userInput;
    int bikeId;
    int dealershipId;
    double price = 150.67;

    std::cout << "What bike to assign?: " << std::endl;
    for (int i = 0; i < controller.bikes.size(); i++) {
        std::cout << i + 1 << " - " <<
        controller.bikes[i] << std::endl;
    }
    std::cin >> bikeId;
    std::cin.clear();
    bikeId--;

    std::cout << "Which dealership?: " << std::endl;
    for (int i = 0; i < controller.bikeDealerships.size(); i++) {
        std::cout << i + 1 << " - " <<
        controller.bikeDealerships[i].getName() << std::endl;
    }
    std::cin >> dealershipId;
    std::cin.clear();
    dealershipId--;

    std::cout << "For what price? (example: 150.67): " << std::endl;
    std::cin >> price;
    std::cin.clear();

    std::vector<std::tuple<Bike, double>> catalog = controller.bikeDealerships[dealershipId].getCatalog();
    catalog.push_back(std::tuple<Bike, double>{controller.bikes[bikeId], price});
    controller.bikeDealerships[dealershipId].setCatalog(catalog);
    controller.bikes.erase(controller.bikes.begin() + bikeId);
    return;
}

void UIManager::manageLicenses(StateController& controller) {
    std::cout << "Managing licenses of which player?: " << std::endl;
    for (int i = 0; i < controller.players.size(); i++) {
        std::cout << i + 1 << " - " << controller.players[i] << std::endl;
    }
    int playerId;
    std::cin >> playerId;
    playerId--;
    std::cin.clear();

    std::cout << "Current licenses:" << std::endl;
    for (int i = 0; i < controller.players[playerId].getLicenses().size(); i++) {
        std::cout << controller.players[playerId].getLicenses()[i] << std::endl;
    }

    std::cout << "1 - Add license, 0 - Remove license:" << std::endl;
    int userInput;
    std::cin >> userInput;
    std::cin.clear();

    std::cout << "What license? (A, B):" << std::endl;
    std::string license;
    std::cin >> license;
    std::cin.clear();
    if (userInput) {
        controller.players[playerId].addLicense(license);
    } else {
        controller.players[playerId].removeLicense(license);
    }
    return;
}

void UIManager::purchaseVehicle(StateController& controller) {
    std::cout << "What dealership type?:" << std::endl;
    int userInput;
    std::cout << "1 - Car, 0 - Bike" << std::endl;
    std::cin >> userInput;
    std::cin.clear();
    if (userInput) {
        std::cout << "Which car dealership?:" << std::endl;
        for (int i = 0; i < controller.carDealerships.size(); i++) {
            std::cout << i + 1 << " - " << controller.carDealerships[i].getName() << std::endl;
        }
        int dealershipId;
        std::cin >> dealershipId;
        dealershipId--;
        std::cin.clear();
        std::cout << "Which car?: " << std::endl;
        for (int i = 0; i < controller.carDealerships[dealershipId].getCatalog().size(); i++) {
            std::cout << i + 1 << " - " << std::get<0>(controller.carDealerships[dealershipId].getCatalog()[i]) << " | " <<
            std::get<1>(controller.carDealerships[dealershipId].getCatalog()[i]) << std::endl;
        }
        int vehicleId;
        std::cin >> vehicleId;
        vehicleId--;
        std::cin.clear();
        std::cout << "For which player?:" << std::endl;
        for (int i = 0; i < controller.players.size(); i++) {
            std::cout << i + 1 << " - " << controller.players[i] << std::endl;
        }
        int playerId;
        std::cin >> playerId;
        playerId--;
        std::cin.clear();
        Car car = std::get<0>(controller.carDealerships[dealershipId].getCatalog()[vehicleId]);
        controller.carDealerships[dealershipId].sell(car, controller.players[playerId]);
    } else {
        std::cout << "Which bike dealership?:" << std::endl;
        for (int i = 0; i < controller.bikeDealerships.size(); i++) {
            std::cout << i + 1 << " - " << controller.bikeDealerships[i].getName() << std::endl;
        }
        int dealershipId;
        std::cin >> dealershipId;
        dealershipId--;
        std::cin.clear();
        std::cout << "Which bike?: " << std::endl;
        for (int i = 0; i < controller.bikeDealerships[dealershipId].getCatalog().size(); i++) {
            std::cout << i + 1 << " - " << std::get<0>(controller.bikeDealerships[dealershipId].getCatalog()[i]) << " | " <<
            std::get<1>(controller.bikeDealerships[dealershipId].getCatalog()[i]) << std::endl;
        }
        int vehicleId;
        std::cin >> vehicleId;
        vehicleId--;
        std::cin.clear();
        std::cout << "For which player?:" << std::endl;
        for (int i = 0; i < controller.players.size(); i++) {
            std::cout << i + 1 << " - " << controller.players[i] << std::endl;
        }
        int playerId;
        std::cin >> playerId;
        playerId--;
        std::cin.clear();
        
        Bike bike = std::get<0>(controller.bikeDealerships[dealershipId].getCatalog()[vehicleId]);
        controller.bikeDealerships[dealershipId].sell(bike, controller.players[playerId]);
    }
    return;
}