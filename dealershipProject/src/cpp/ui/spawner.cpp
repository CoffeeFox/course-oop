#include <iostream>
#include "spawner.h"
#include "bike.h"
#include "car.h"
#include "player.h"
#include "dealership.h"
#include "state_controller.h"


void UISpawner::showMenu(StateController& controller) {
    while (true) {
        system("clear");
        std::cout << "1 - Spawn player\n" <<
        "2 - Spawn car\n" << "3 - Spawn bike\n" <<
        "4 - Spawn dealership\n" <<
        "0 - Return to menu" << std::endl;

        int response;
        std::cin >> response;
        std::cin.clear();

        switch(response) {
            case 1:
                this->createPlayer(controller);
                break;
            case 2:
                this->createCar(controller);
                break;
            case 3:
                this->createBike(controller);
                break;
            case 4:
                this->createDealership(controller);
                break;
            case 0:
                return;
        }
    }
    return;
}

void UISpawner::createPlayer(StateController& controller) {
    std::string name;
    std::vector<Car> cars;
    std::vector<Bike> bikes;
    std::vector<std::string> licenses;
    double money;

    std::cout << "Enter name of the player: ";
    std::cin >> name;
    Player player(name);
    player.setMoney(1000);
    controller.players.push_back(player);
    return;
}

void UISpawner::createCar(StateController& controller) {
    std::string brand;
    std::string model;
    std::string plateNumber;
    int condition;
    std::string color;
    std::string drive;
    
    std::cout << "Enter brand of the car: ";
    std::cin >> brand;
    std::cin.clear();
    std::cout << "Enter model of the car: ";
    std::cin >> model;
    std::cin.clear();
    std::cout << "Enter plate number of the car: ";
    std::cin >> plateNumber;
    std::cin.clear();
    std::cout << "Enter condition of the car (0-100%): ";
    std::cin >> condition;
    std::cin.clear();
    std::cout << "Enter color the car: ";
    std::cin >> color;
    std::cin.clear();
    std::cout << "Enter drive of the car (FWD, AWD, RWD): ";
    std::cin >> drive;
    std::cin.clear();

    Car car(brand, model, "No owner", plateNumber, condition, color, drive);
    controller.cars.push_back(car);
    return;
}

void UISpawner::createBike(StateController& controller) {
    std::string brand;
    std::string model;
    std::string plateNumber;
    int condition;
    std::string color;
    std::string type;
    
    std::cout << "Enter brand of the bike: ";
    std::cin >> brand;
    std::cin.clear();
    std::cout << "Enter model of the bike: ";
    std::cin >> model;
    std::cin.clear();
    std::cout << "Enter plate number of the bike: ";
    std::cin >> plateNumber;
    std::cin.clear();
    std::cout << "Enter condition of the bike (0-100%): ";
    std::cin >> condition;
    std::cin.clear();
    std::cout << "Enter color the bike: ";
    std::cin >> color;
    std::cin.clear();
    std::cout << "Enter type of the bike (Cruiser, Chopper, etc.): ";
    std::cin >> type;
    std::cin.clear();

    Bike bike(brand, model, "No owner", plateNumber, condition, color, type);
    controller.bikes.push_back(bike);
    return;
}

void UISpawner::createDealership(StateController& controller) {
    std::cout << "1 - Car Dealership\n0 - Bike Dealership" << std::endl;
    int choise;
    std::cin >> choise;
    std::cin.clear();
    if (choise) {
        std::string name;
        std::cout << "Enter the name of the dealership: ";
        std::cin >> name;
        std::cin.clear();
        Dealership<Car> dealership(name);
        controller.carDealerships.push_back(dealership);
    } else {
        std::string name;
        std::cout << "Enter the name of the dealership: ";
        std::cin >> name;
        std::cin.clear();
        Dealership<Bike> dealership(name);
        controller.bikeDealerships.push_back(dealership);
    }
    return;
}