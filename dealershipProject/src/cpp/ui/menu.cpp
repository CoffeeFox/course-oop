#include <iostream>
#include "menu.h"
#include "spawner.h"
#include "remover.h"
#include "manager.h"
#include "state_controller.h"


void UIMenu::showMenu(StateController& controller) {
    while (true) {
        system("clear");
        std::cout << "Players - " << controller.players.size() << "\n" <<
        "Cars - " << controller.cars.size() << "\n" <<
        "Bikes - " << controller.bikes.size() << "\n" <<
        "Car Dealerships - " << controller.carDealerships.size() << "\n" <<
        "Bike Dealerships - " << controller.bikeDealerships.size() <<
        "\n\n" << std::endl;
        std::cout << "1 - Call spawner\n" <<
        "2 - Call remover\n" << "3 - Call manager\n" <<
        "0 - Quit the app" << std::endl;

        int response;
        std::cin >> response;
        std::cin.clear();

        switch(response) {
            case 1:
                this->callSpawner(controller);
                break;
            case 2:
                this->callRemover(controller);
                break;
            case 3:
                this->callManager(controller);
                break;
            case 0:
                system("clear");
                std::cout << "Quit!" << std::endl;
                Database db;
                db.save(controller);
                std::cout << "Progress saved!" << std::endl;
                std::exit(0);
        }
    }
    return;
}

void UIMenu::callSpawner(StateController& controller) {
    UISpawner spawner;
    spawner.showMenu(controller);
    return;
}

void UIMenu::callRemover(StateController& controller) {
    UIRemover remover;
    remover.showMenu(controller);
    return;
}

void UIMenu::callManager(StateController& controller) {
    UIManager manager;
    manager.showMenu(controller);
    return;
}