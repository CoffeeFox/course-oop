#include <string>
#include <iostream>
#include <vector>
#include "player.h"


Player::Player()
: name("Unnamed"), cars({}), bikes({}), money(0) {}

Player::Player(std::string userName) 
: name(userName),  cars({}), bikes({}), money(0) {}

Player::Player(std::string userName, std::vector<Car> cars,
    std::vector<Bike> bikes, double money,
    std::vector<std::string> licenses)
: name(userName), cars(cars), bikes(bikes), money(money), licenses(licenses) {}

std::string Player::getName() const {return name;}
double Player::getMoney() const {return money;}
std::vector<Car> Player::getCars() const {return cars;}
std::vector<Bike> Player::getBikes() const {return bikes;}
std::vector<std::string> Player::getLicenses() const {return licenses;}

int Player::setName(std::string newName) {this->name = newName; return 0;}
int Player::setCars(std::vector<Car> newVehicles) {this->cars = newVehicles; return 0;}
int Player::setBikes(std::vector<Bike> newVehicles) {this->bikes = newVehicles; return 0;}
int Player::setMoney(double newMoney) {
    if (newMoney < 0) {
        return 1;
    } else {
        this->money = newMoney;
        return 0;
    }
}
int Player::setLicenses(std::vector<std::string> newLicenses) {this->licenses = newLicenses; return 0;}


int Player::addVehicle(Bike bike) {
    bikes.push_back(bike); 
    return 0;
}

int Player::addVehicle(Car car) {
    cars.push_back(car); 
    return 0;
}

int Player::removeVehicle (Bike bike) {
    int i = 0;
    for (; i < bikes.size(); i++) {
        if (bikes[i] == bike) {
            break;
        }
    }
    bikes.erase(bikes.begin()+i);
    return 0;
}
int Player::removeVehicle(Car car) {
    int i = 0;
    for (; i < cars.size(); i++) {
        if (cars[i] == car) {
            break;
        }
    }
    cars.erase(cars.begin()+i);
    return 0;
}

int Player::addMoney(double m) {
    this->setMoney(money + m);
    return 0;
}

int Player::subtractMoney(double m) {
    this->setMoney(money - m);
    return 0;
}

int Player::addLicense(std::string license) {licenses.push_back(license); return 0;}
int Player::removeLicense(std::string license) {
    int i = 0;
    for (; i < licenses.size(); i++) {
        if (licenses[i] == license) {
            break;
        }
    }
    licenses.erase(licenses.begin()+i);
    return 0;
}

std::ostream& operator<<(std::ostream& stream, const Player& pl) {
    return stream << pl.getName();
}