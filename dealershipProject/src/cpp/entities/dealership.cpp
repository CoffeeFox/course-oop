#include <iostream>
#include <string>
#include <vector>
#include <tuple>
#include <typeinfo>
#include "player.h"
#include "dealership.h"
#include "car.h"


template<typename T>
Dealership<T>::Dealership() 
: name("Untitled dealership"), catalog({}) {}

template<typename T>
Dealership<T>::Dealership(std::string enteredName)
: name(enteredName), catalog({}) {}

template<typename T>
Dealership<T>::Dealership(std::string name, std::vector<std::tuple<T, double>> catalog)
: name(name), catalog(catalog) {}

template<typename T>
std::vector<std::tuple<T, double>> Dealership<T>::getCatalog() const {return catalog;}

template<typename T>
int Dealership<T>::setCatalog(std::vector<std::tuple<T, double>> enteredVehicles)
{this->catalog = enteredVehicles; return 0;}

template<typename T>
int Dealership<T>::sell(T vehicle, Player& player) {
    if (!isPlayerAllowed(player)) {
        return 1;
    }
    int i = 0;
    for (;i < catalog.size(); i++) {
        if (std::get<0>(catalog[i]) == vehicle) {
            break;
        }
    }
    vehicle.setOwnerName(player.getName());
    player.addVehicle(vehicle);
    player.subtractMoney(std::get<1>(catalog[i]));
    catalog.erase(catalog.begin()+i);
    return 0;
}

template<typename T>
int Dealership<T>::setName(std::string enteredName) {this->name = enteredName; return 0;}

template<typename T>
std::string Dealership<T>::getName() const {return name;}

template<typename T>
bool Dealership<T>::isPlayerAllowed(Player& player) {
    std::vector<std::tuple<Car, double>> checker;
    bool isAllowed = false;
    std::string requiredLicense;
    if (typeid(catalog) == typeid(checker)) {
        requiredLicense = "B";
    } else {
        requiredLicense = "A";
    }

    for (int i = 0; i < player.getLicenses().size(); i++) {
        if (player.getLicenses()[i] == requiredLicense) {
            isAllowed = true;
            break;
        }
    }
    return isAllowed;
}

template class Dealership<Car>;
template class Dealership<Bike>;
