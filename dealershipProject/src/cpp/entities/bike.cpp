#include <iostream>
#include <string>
#include "bike.h"


Bike::Bike()
: type("Unknown type") {}

Bike::Bike(std::string brand, std::string model, std::string ownerName,
std::string plateNumber, int enteredCondition, 
std::string color, std::string type)
: type(type), Vehicle(brand, model, ownerName, 
plateNumber, enteredCondition, color) {}

std::string Bike::getType() const {return this->type;}
int Bike::setType(std::string newType) {this->type=newType; return 0;}

std::ostream& Bike::print(std::ostream& stream) const {
    stream << this->getBrand() << " " << this->getModel() <<
        " [" << this->getPlateNumber() << "] " << "[" <<
        this->getType() << "]";
    return stream;
}

bool operator==(Bike v1, Bike v2) {
    if ((v1.getBrand() == v2.getBrand()) and 
        (v1.getModel() == v2.getModel()) and 
        (v1.getOwnerName() == v2.getOwnerName()) and
        (v1.getPlateNumber() == v2.getPlateNumber()) and
        (v1.getCondition() == v2.getCondition()) and
        (v1.getColor() == v2.getColor()) and (v1.getType() == v2.getType())) {return true;}
    else {return false;}
}