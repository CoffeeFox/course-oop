#include <iostream>
#include <string>
#include "vehicle.h"


Vehicle::Vehicle()
: brand("Unknown brand"), model("Unknown model"), ownerName("Unknown owner"),
plateNumber("Unknown plate number"), condition(-1),
color("Unknown color") {}

Vehicle::Vehicle(std::string brand, std::string model, std::string ownerName,
std::string plateNumber, int enteredCondition, std::string color) 
: brand(brand), model(model),
ownerName(ownerName), plateNumber(plateNumber), color(color) {
    if ((enteredCondition <= 100) and (enteredCondition >= 0)) {
        condition = enteredCondition;
    } else {
        condition = 100;
    }
}

std::ostream& Vehicle::print(std::ostream& stream) const {
    stream << this->getBrand() << " " << this->getModel() <<
        " [" << this->getPlateNumber() << "]";
    return stream;
}

std::ostream& operator<<(std::ostream& stream, const Vehicle& vic) {
    return vic.print(stream);
}

std::string Vehicle::getBrand() const {return brand;}
std::string Vehicle::getModel() const {return model;}
std::string Vehicle::getOwnerName() const {return ownerName;}
std::string Vehicle::getPlateNumber() const {return plateNumber;}
int Vehicle::getCondition() const {return condition;}
std::string Vehicle::getColor() const {return color;}

int Vehicle::setBrand(std::string newBrand) {this->brand = newBrand; return 0;}
int Vehicle::setModel(std::string newModel) {this->model = newModel; return 0;}
int Vehicle::setOwnerName(std::string newOwnerName) {this->ownerName = newOwnerName; return 0;}
int Vehicle::setPlateNumber(std::string newPlateNumber) {this->plateNumber = newPlateNumber; return 0;}
int Vehicle::setCondition(int newCondition) {
    if ((newCondition <= 100) and (newCondition >= 0)) {
        this->condition = newCondition;
        return 0;
    } else {
        return 1;
    }
}

int Vehicle::setColor(std::string newColor) {this->color = newColor; return 0;}

bool operator==(Vehicle v1, Vehicle v2) {
    if ((v1.brand == v2.brand) and 
        (v1.model == v2.model) and 
        (v1.ownerName == v2.ownerName) and
        (v1.plateNumber == v2.plateNumber) and
        (v1.condition == v2.condition) and
        (v1.color == v2.color)) {return true;}
    else {return false;}
}