#include <iostream>
#include <string>
#include "car.h"


Car::Car()
: drive("Unknown drive") {}

Car::Car(std::string brand, std::string model, std::string ownerName,
std::string plateNumber, int enteredCondition, 
std::string color, std::string drive)
: drive(drive), Vehicle(brand, model, ownerName, 
plateNumber, enteredCondition, color) {}

std::string Car::getDrive() const {return this->drive;}
int Car::setDrive(std::string newDrive) {this->drive=newDrive; return 0;}

std::ostream& Car::print(std::ostream& stream) const {
    stream << this->getBrand() << " " << this->getModel() <<
        " [" << this->getPlateNumber() << "] " << "[" <<
        this->getDrive() << "]";
    return stream;
}

bool operator==(Car v1, Car v2) {
    if ((v1.getBrand() == v2.getBrand()) and 
        (v1.getModel() == v2.getModel()) and 
        (v1.getOwnerName() == v2.getOwnerName()) and
        (v1.getPlateNumber() == v2.getPlateNumber()) and
        (v1.getCondition() == v2.getCondition()) and
        (v1.getColor() == v2.getColor()) and (v1.getDrive() == v2.getDrive())) {return true;}
    else {return false;}
}