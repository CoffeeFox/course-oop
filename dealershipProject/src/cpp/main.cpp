#include "menu.h"
#include "state_controller.h"
#include "database.h"


class Application {
public:
    int run() {
        Database db;
        int userInput;
        std::cout << "1 - Load demo database\n2 - Load your created database\n0 - Start a clean database\n" <<
        std::endl;
        std::cin >> userInput;
        std::cin.clear();
        StateController controller;
        switch (userInput) {
            case 1:
                controller = db.load("../demo.json");
                break;
            case 2:
                controller = db.load("data.json");
                break;
            case 0:
            default:
                break;
        }
        UIMenu menu;
        menu.showMenu(controller);
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}