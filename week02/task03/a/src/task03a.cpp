#include <iostream>


class Application {
public:
    int maxValues(double array[], int size) {
        double max = array[0];
        int count = 1;
        for (int i = 1; i < size; i++) {
            if (max < array[i]) {
                max = array[i];
                count = 1;
            } else if (max == array[i]) {
                count++;
            }
        }
        return count;
    }

    int run() {
        double array[100];
        int amount;
        std::cout << "How many elements are to be inputted? ";
        std::cin >> amount;
        std::cin.clear();
        for (int i = 0; i < amount; i++) {
            std::cout << "Enter element number " << i + 1 << std::endl;
            std::cin >> array[i];
        }
        int maxValuesCount = maxValues(array, amount);
        std::cout << "There are " << maxValuesCount << " values equal to max"
            << std::endl;
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
