#include <iostream>


void merge(double *array, int left, int middle, int right) {
   int i, j, k;
   int nLeft = middle-left+1;
   int nRight = right-middle;
   double larr[nLeft], rarr[nRight];

   for (i = 0; i < nLeft; i++)
      larr[i] = array[left+i];
   for (j = 0; j < nRight; j++)
      rarr[j] = array[middle+1+j];
   i = 0; j = 0; k = left;
   while (i < nLeft && j<nRight) {
      if (larr[i] <= rarr[j]) {
         array[k] = larr[i];
         i++;
      } else {
         array[k] = rarr[j];
         j++;
      }
      k++;
   }
   while (i<nLeft) {
      array[k] = larr[i];
      i++; k++;
   }
   while (j<nRight) {
      array[k] = rarr[j];
      j++; k++;
   }
}

void mergeSort(double *array, int left, int right) {
   int middle;
   if (left < right) {
      middle = left+(right-left)/2;
      mergeSort(array, left, middle);
      mergeSort(array, middle+1, right);
      merge(array, left, middle, right);
   }
}


class Application {
public:
        int run() {
        double array[100];
        int amount;
        std::cout << "How many elements are to be inputted? ";
        std::cin >> amount;
        std::cin.clear();
        for (int i = 0; i < amount; i++) {
            std::cout << "Enter element number " << i + 1 << std::endl;
            std::cin >> array[i];
        }
        std::cout << "\nUnsorted:\n";
        for (int i = 0; i < amount; i++) {
            std::cout << array[i] << std::endl;
        }
        mergeSort(array, 0, amount-1);
        std::cout << "\nSorted:\n";
        for (int i = 0; i < amount; i++) {
            std::cout << array[i] << std::endl;
        }
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
