#include <iostream>


class Application {
public:
    void drawPyramid(int height) {
        if (height > 1) {
            int width = height * 2;
            int space = height - 1;
            int symbols = 1;
            while (space > -1) {
                for (int i = 0; i < space; i++) {
                    std::cout << " "; 
                };
                for (int i = 0; i < symbols; i++) {
                    std::cout << "#";
                };
                std::cout << "  ";
                for (int i = 0; i < symbols; i++) {
                    std::cout << "#";
                };
                for (int i = 0; i < space; i++) {
                    std::cout << " ";
                };
                std::cout << std::endl;
                space--;
                symbols++;
            };
        };
    }
    
    int run() {
        std::cout << "Введите высоту пирамиды: ";
        int height;
        std::cin >> height;
        drawPyramid(height);
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}

