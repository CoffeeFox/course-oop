#include <iostream>
#include <cmath>


class Application {
public:
    double sqrtAlt(int value, double accuracy) {
        double xCurrent = 3;
        double xNext = 0.5 * (xCurrent + value / xCurrent);
        int cycleGuard = 0;
        while (abs(xNext - xCurrent) > accuracy and cycleGuard < 50) {
           xCurrent = xNext;
           xNext = 0.5 * (xCurrent + value / xCurrent);
           cycleGuard++;
        }
        return xNext;
    }

    int run() {
        std::cout << "Введите число: ";
        int value;
        std::cin >> value;
        std::cout << "Квадратный корень вашего числа равен: "
            << sqrtAlt(value, 10^-100) << std::endl;
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
