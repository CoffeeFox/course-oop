#include <iostream>


class Application {
public:
    void printSimpleValues(int limit) {
        int array[limit];
        for (int i = 0; i < limit + 1; i++) {
            array[i] = i;
        }
        for (int i = 2; i < limit + 1; i++) {
            if (array[i] != 0) {
                std::cout << array[i] << std::endl;
                for (int j = i*i; j < limit + 1; j += i) {
                    array[j] = 0;
                }
            }
        }
        return;
    }

    int run() {
        std::cout << "Введите количество обрабатываемых чисел: ";
        int limit;
        std::cin >> limit;
        printSimpleValues(limit);
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
