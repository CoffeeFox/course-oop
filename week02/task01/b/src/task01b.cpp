#include <iostream>


class Application {
public:
    int getDays(int month) {
        switch (month) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                return 31;
            case 2:
                return 28;
            case 4:
            case 6:
            case 9:
            case 11:
                return 30;
            default:
                return 0;
        }
    }

    int run() {
        std::cout << "Введите номер месяца: ";
        int month;
        std::cin >> month;
        int days = getDays(month);
        switch (days) {
        case 31:
            std::cout << days << " день" << std::endl;
            break;
        case 30:
        case 28:
            std::cout << days << " дней" << std::endl;
            break;
        default:
            std::cout << "Неверно указан месяц" << std::endl;
            break;
        }
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
