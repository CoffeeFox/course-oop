#include <iostream>


class Application {
public:
    bool isGoingToPlay() {
        std::cout << "Введите текущие условия:\n";
        bool isSunday;
        bool isGoodWeather;
        enum TEMPERATURE {
            HOT = 1,
            WARM = 2,
            COLD = 3,
        };
        enum SEDIMENTS {
            CLEAR = 1,
            CLOUDY = 2,
            RAIN = 3,
            SNOW = 4,
            HAIL = 5,
        };
        bool isWindy;
        enum HUMIDITY {
            HIGH = 1,
            LOW = 0,
        };

        int userInput;
        std::cout << "Сегодня воскресенье?" << std::endl;
        std::cin >> isSunday;
        std::cin.clear();
        std::cout << "Какая сегодня температура?" 
        << "1 - жарко, 2 - тепло, 3 - холодно" << std::endl;
        std::cin >> userInput;
        TEMPERATURE temperature = (TEMPERATURE)userInput;
        std::cin.clear();
        std::cout << "Какая сегодня погода? 1 - ясно, 2 - облачно, "
        << "3 - дождь, 4 - снег, 5 - град" << std::endl;
        std::cin >> userInput;
        SEDIMENTS sediments = (SEDIMENTS)userInput;
        std::cin.clear();
        std::cout << "Сегодня ветрено? 1 - да, 0 - нет" << std::endl;
        std::cin >> isWindy;
        std::cin.clear();
        std::cout << "Какая влажность? 1 - высокая влажность, "
            << "0 - низкая влажность"  << std::endl;
        std::cin >> userInput;
        HUMIDITY humidity = (HUMIDITY)userInput;
        std::cin.clear();
        if (humidity == HIGH and (temperature == WARM or temperature == HOT)
            and (sediments == CLEAR or sediments == CLOUDY)
            and !isWindy) {
            isGoodWeather = true;
            } else {
            isGoodWeather = false;
            }
        if (isSunday and isGoodWeather) {
            return true;
        } else {
            return false;
        }
    }


    int run() {
        bool isTimeToPlay = isGoingToPlay();
        if (isTimeToPlay) {
            std::cout << "Да" << std::endl;
        } else {
            std::cout << "Нет" << std::endl;
        }
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
