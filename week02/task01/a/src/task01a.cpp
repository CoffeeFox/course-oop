#include <iostream>


class Application {
public:
    void printAge(int age) {
        int lastDigit = age;
        while (lastDigit >=10) {
            lastDigit = lastDigit % 10;
        }
        switch (lastDigit) {
            case 1:
                std::cout << age << " год" << std::endl;
                break;
            case 2:
            case 3:
            case 4:
                std::cout << age << " года" << std::endl;
                break;
            default:
                std::cout << age << " лет" << std::endl;
        }
        return;
    }

    int run() {
        int age;
        std::cout << "Введите ваш возраст: ";
        std::cin >> age;
        printAge(age);
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
