#include <iostream>
#include <string>
#include "mergeSort.cpp"


class Application {
public:
        int run() {
            int amount = 5;
            // int
            int arrayInt[] = {12, 11, 10, 9, 8};
            std::cout << "\nUnsorted:\n";
            for (int i = 0; i < amount; i++) {
                std::cout << arrayInt[i] << std::endl;
            }
            mergeSort(arrayInt, 0, amount-1);
            std::cout << "\nSorted:\n";
            for (int i = 0; i < amount; i++) {
                std::cout << arrayInt[i] << std::endl;
            }

            // double
            double arrayDub[] = {12, 11, 10, 9, 8};
            std::cout << "\nUnsorted:\n";
            for (int i = 0; i < amount; i++) {
                std::cout << arrayDub[i] << std::endl;
            }
            mergeSort(arrayDub, 0, amount-1);
            std::cout << "\nSorted:\n";
            for (int i = 0; i < amount; i++) {
                std::cout << arrayDub[i] << std::endl;
            }

            // string
            std::string arrayStr[] = {"foo", "bar", "fish", "chips", "clanka"};
            std::cout << "\nUnsorted:\n";
            for (int i = 0; i < amount; i++) {
                std::cout << arrayStr[i] << std::endl;
            }
            mergeSort(arrayStr, 0, amount-1);
            std::cout << "\nSorted:\n";
            for (int i = 0; i < amount; i++) {
                std::cout << arrayStr[i] << std::endl;
            }
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
