template<typename T>
void merge(T *array, int left, int middle, int right) {
   int i, j, k;
   int nLeft = middle-left+1;
   int nRight = right-middle;
   T larr[nLeft], rarr[nRight];

   for (i = 0; i < nLeft; i++)
      larr[i] = array[left+i];
   for (j = 0; j < nRight; j++)
      rarr[j] = array[middle+1+j];
   i = 0; j = 0; k = left;
   while (i < nLeft && j<nRight) {
      if (larr[i] <= rarr[j]) {
         array[k] = larr[i];
         i++;
      } else {
         array[k] = rarr[j];
         j++;
      }
      k++;
   }
   while (i<nLeft) {
      array[k] = larr[i];
      i++; k++;
   }
   while (j<nRight) {
      array[k] = rarr[j];
      j++; k++;
   }
}

template<typename T>
void mergeSort(T *array, int left, int right) {
   int middle;
   if (left < right) {
      middle = left+(right-left)/2;
      mergeSort(array, left, middle);
      mergeSort(array, middle+1, right);
      merge(array, left, middle, right);
   }
}