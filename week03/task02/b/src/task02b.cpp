#include <iostream>
#include <regex>


class Application {
public:
    bool isValidIPv6(std::string userInput) {
        static const std::regex reg("(([a-z]|[0-9]){4})(:([a-z]|[0-9]){4}){7}");
        return std::regex_match(userInput, reg);
    }

    int run() {
        std::cout << "Input an IPv6" << std::endl;
        std::string userInput;
        std::cin >> userInput;
        if (isValidIPv6(userInput)) {
            std::cout << "The IP is valid" << std::endl;
        } else {
            std::cout << "The IP is not valid" << std::endl;
        }
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
