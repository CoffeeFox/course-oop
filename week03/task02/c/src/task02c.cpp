#include <iostream>
#include <regex>
#include <string>


class Application {
public:
    std::string replaceRepeating(std::string userInput) {
        std::regex reg("(.)\\1\\1+");
        return std::regex_replace(userInput, reg, "$1");
    }

    int run() {
        std::cout << "Input a string" << std::endl;
        std::string userInput;
        std::cin >> userInput;
        std::cout << replaceRepeating(userInput) << std::endl;
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
