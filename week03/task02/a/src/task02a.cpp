#include <iostream>
#include <regex>


class Application {
public:
    bool isValidEmail(std::string userInput) {
        static const std::regex mailRegex("\\w+@\\w+\\.\\w+");
        return std::regex_match(userInput, mailRegex);
    }

    int run() {
        std::cout << "Input an email adress" << std::endl;
        std::string userInput;
        std::cin >> userInput;
        if (isValidEmail(userInput)) {
            std::cout << "The email is valid" << std::endl;
        } else {
            std::cout << "The email is not valid" << std::endl;
        }
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
