#include <iostream>
#include <string>


class Application {
public:
    int findAmount(std::string userInput, std::string pattern) {
        int amount = 0;
        for (int i = 0; (i = userInput.find(pattern, i)) != std::string::npos; i++) {
            amount++;
        }
        return amount;
    }

    int run() {
        std::cout << "Введите строку, в которой будем искать: ";
        std::string userInput;
        std::cin >> userInput;
        std::string pattern;
        std::cout << "Введите строку, которую будем искать: ";
        std::cin >> pattern;

        int amount = findAmount(userInput, pattern);
        std::cout << "Встретили вхождений: " << amount << std::endl;
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
