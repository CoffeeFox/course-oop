#include <iostream>
#include <cmath>


double entropyShennon(std::string userInput) {
    double entropy = 0;
    double p;
    double freq[256] = {0};
    for (int i = 0; i < userInput.size(); i++) {
        freq[(unsigned char)userInput[i]]++;
    }
    for (int i = 0; i < userInput.size(); i++) {
        p = double(freq[(unsigned char)userInput[i]]) / double(userInput.size());
        entropy += -p * log2(p);
    }
    return entropy;
}


class Application {
public:
    int run() {
        std::cout << "Введите строку: ";
        std::string userInput;
        std::cin >> userInput;
        std::cout << "Энтропия строки равна " << entropyShennon(userInput) << std::endl;
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
