#include <iostream>

class Sensor {
private: 
    int id;
    double value;
public:
    Sensor()
    : id(-1), value(0) {}

    Sensor(int id, double value)
    : id(id), value(value) {};

    void print() {
        std::cout << id << " " << value << ".0" << std::endl;
    }

    int getId() {
        return id;
    }

    double getValue() {
        return value;
    }

    void setId(int newId) {
        id = newId;
        return;
    }

    void setValue(double newValue) {
        value = newValue;
        return;
    }

    void setAll (int newId, double newValue) {
        id = newId;
        value = newValue;
        return;
    }

    friend void processString(std::string userInput, bool sortMethod);
};


void processStringCycle(std::string* temp, std::string*userInput, Sensor* array, int* counter) {
    // Separate 1 sensor and delete it from the main string
    *temp = userInput->substr(0, userInput->find("@"));
    userInput->erase(0, userInput->find("@") + 1);
    // Add a sensor to array
    array[*counter].setAll(std::stoi(temp->substr(0, 2)),
        std::stoi(temp->substr(2, temp->back())));
    (*counter)++;
    return;
}


void processString(std::string userInput, bool sortMethod) {
    Sensor array[40];
    int counter = 0;
    { // Fill the array
        std::string temp;
        while (userInput.find("@") != std::string::npos) {
            processStringCycle(&temp, &userInput, array, &counter);
        }
        processStringCycle(&temp, &userInput, array, &counter);
    }

    // Sort and print
    Sensor tempArray[40];
    bool condition;
    while (counter > 0) {
        int indexMin = 0;
        for (int i = 0; i < counter; i++) {
            if (sortMethod) {
                condition = array[i].id < array[indexMin].id;
            } else {
                condition = array[i].value < array[indexMin].value;
            }
            if (condition and (array[i].id != -1)) {
                indexMin = i;
            }
        }
        array[indexMin].print();
        array[indexMin] = array[counter-1];
        counter--;
    }
    return;
}


class Application {
public:
    int run() {
        std::string userInput;
        std::cout << "Показания:" << std::endl;
        std::cin >> userInput;
        bool sortMethod;
        std::cout << "Как отсортировать вывод? 1 - по номерам датчиков, 0 - по показаниям"
            << std::endl;
        std::cin >> sortMethod;
        processString(userInput, sortMethod);
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
