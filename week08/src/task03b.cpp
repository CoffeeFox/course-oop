#include "headers/task03b.h"


int main() {
    Application app;
    std::map<std::string, std::vector<std::string>> result = app.run();
    std::cout << result["commonLangs"].size() << std::endl;
    printVector(result["commonLangs"]);
    std::cout << result["uniqueLangs"].size() << std::endl;
    printVector(result["uniqueLangs"]);
    return 0;
}
