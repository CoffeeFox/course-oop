#include <gtest/gtest.h>
#include "../headers/task03b.h"


TEST(TestB, TestB) {
  STUDENTS students;
  students.push_back(std::vector<std::string> {"Russian", "English", "Japanese"});
  students.push_back(std::vector<std::string> {"Russian", "English"});
  students.push_back(std::vector<std::string> {"English"});
  Application app;
  app.setStudents(students);
  std::map<std::string, std::vector<std::string>> result = app.run();

  std::vector<std::string> expectedCommonLangs{"English"};
  std::vector<std::string> expectedUniqueLangs{"English", "Japanese", "Russian"};
  EXPECT_EQ(result["commonLangs"].size(), 1);
  EXPECT_EQ(result["uniqueLangs"].size(), 3);
  EXPECT_EQ(result["commonLangs"], expectedCommonLangs);
  EXPECT_EQ(result["uniqueLangs"], expectedUniqueLangs);
}