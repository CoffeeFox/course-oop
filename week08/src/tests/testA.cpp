#include <gtest/gtest.h>
#include <set>
#include "../headers/task03a.h"


TEST(TestA, TestA) {
    Application app;
    std::set<int> answer;
    answer.insert(23);
    answer.insert(34);
    answer.insert(54);
    std::string userInput = "23, 23, 34, 34, 54, 54";
    EXPECT_EQ(app.run(userInput), answer);
}