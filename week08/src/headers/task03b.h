#pragma once
#include <iostream>
#include <algorithm>
#include <string>
#include <vector>
#include <map>


typedef std::vector<std::vector<std::string>> STUDENTS;


void printVector(std::vector<std::string> v) {
    for (int i = 0; i < v.size(); i++) {
            std::cout << v[i] << std::endl;
    }
    return;
}


class Application {
private:
    STUDENTS students;
public:
    void setStudents(STUDENTS userInput) {
        students = userInput;
        return;
    }

    STUDENTS getStudents() {
        int studentsAmount;
        std::string userInput;
        std::cin >> studentsAmount;
        STUDENTS students;
        for (int i = 0; i < studentsAmount; i++) {
            int languageAmount;
            std::cin >> languageAmount;
            std::vector<std::string> temp;
            for (int j = 0; j < languageAmount; j++) {
                std::cin >> userInput;
                temp.push_back(userInput);
                std::cin.clear();
            }
            students.push_back(temp);
        }
        return students;
    }

    std::vector<std::string> getCommonLangs(STUDENTS students) {
        std::vector<std::string> langs;
        std::map<std::string, int> langCounts;
        for (int i = 0; i < students.size(); i++) {
            for (int j = 0; j < students[i].size(); j++) {
                langCounts[students[i][j]] += 1;
            }
        }
        for (auto i : langCounts) {
            if (i.second == students.size()) {
                langs.push_back(i.first);
            }
        }
        std::sort (langs.begin(), langs.end());
        return langs;
    }

    std::vector<std::string> getUniqueLangs(STUDENTS students) {
        std::vector<std::string> langs;
        for (int i = 0; i < students.size(); i++) {
            for (int j = 0; j < students[i].size(); j++) {
                if (std::find(langs.begin(), langs.end(), 
                    students[i][j]) != langs.end()) continue;
                langs.push_back(students[i][j]);
            }
        }
        std::sort (langs.begin(), langs.end());
        return langs;
    }

    std::map<std::string, std::vector<std::string>> run() {
        if (students.size() == 0) {
            students = getStudents();
        }

        std::vector<std::string> commonLangs = getCommonLangs(students);
        std::vector<std::string> uniqueLangs = getUniqueLangs(students);

        std::map<std::string, std::vector<std::string>> result;
        result["commonLangs"] = commonLangs;
        result["uniqueLangs"] = uniqueLangs;
        return result;
    }
};