#include <iostream>
#include <vector>
#include "order.cpp"
#include "product.cpp"
#include "customer.cpp"
#include <set>


class Application {
public:
    std::vector<Order> initOrders() {
        Customer customer = Customer(1, "Ivan", 1);
        Product product1 = Product(1, "Pen", "Default", 10.0);
        Product product2 = Product(2, "Paper", "Default", 5.0);
        Product product3 = Product(3, "Notebook", "Default", 20.0);
        std::set<Product> products;
        products.insert(product1);
        products.insert(product2);
        // This one is in Feb 2020, sum is 15
        Order order1 = Order(1, 1580515205, 1580515240, "Ordered", customer, products);
        products.insert(product3);
        // This one is in March 2020, sum is 35
        Order order2 = Order(2, 1589515205, 1589515240, "Ordered", customer, products);
        std::vector<Order> orders;
        orders.push_back(order1);
        orders.push_back(order2);
        return orders;
    }

    double getSum(std::vector<Order> orders, time_t startT, time_t endT) {
        double sum = 0.0;
        for (int i = 0; i < orders.size(); i++) {
            if ((orders[i].getOrderDate() > startT) 
                and (orders[i].getOrderDate() < endT)) {
                std::set<Product> products = orders[i].getProducts();
                for (auto& it: products) {
                    sum += it.getPrice();
                }
            } else {
                continue;
            }
            
        }
        return sum;
    }

    int run() {
        std::vector<Order> orders = initOrders();
        // Time here is bounds of February 2020
        // Check https://www.epochconverter.com/
        double sum = getSum(orders, 1580515200, 1582934400); 
        std::cout << sum << std::endl;
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
