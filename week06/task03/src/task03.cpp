#include <iostream>
#include <cstring>
#include <dirent.h>
#include <vector>
#include <memory>
#include <fstream>


void printVector(std::vector<std::string> v) {
    for (int i = 0; i < v.size(); i++) {
        std::cout << v[i] << std::endl;
    }
    return;
}


class Application {
public:
    std::vector<std::string> getFiles(const std::string& dir) {
        std::vector<std::string> files;
        std::shared_ptr<DIR> directory_ptr(opendir(dir.c_str()), [](DIR* dir){ dir && closedir(dir); });
        struct dirent *dirent_ptr;
        if (!directory_ptr) {
            return files;
        }

        while ((dirent_ptr = readdir(directory_ptr.get())) != nullptr) {
            files.push_back(std::string(dirent_ptr->d_name));
        }
        return files;
    }

    std::string searchForFile(const std::string& filename, const std::string& dir) {
        std::string result = "";
        std::vector<std::string> startDir = getFiles(dir);
        if (startDir.size() > 2) { // If we found something other than . and ..
            for (int i = 0; i < startDir.size(); i++) {
                // Dont go searching the same dir or up the tree
                if ((startDir[i] == ".") or (startDir[i] == "..")) continue;
                // If the file is a text file, look inside it
                if (startDir[i].find(".txt") != std::string::npos) {
                    std::ifstream textFile(dir + '/' + startDir[i]);
                    std::string line;
                    while (getline(textFile, line)) {
                        if (line == filename) {
                            return startDir[i];
                        }
                    }
                }
                // If we found the file, return it right away
                if (startDir[i] == filename) {
                    return filename;
                }
                // If we havent found the file, continue searching
                std::string result = searchForFile(filename, dir + '/' + startDir[i]);
                // If the result of searching indicates
                // that we have found the file, stop searching
                // and return it
                if (result != "") {
                    return startDir[i] + '/' + result;
                }
            }
        }
        return result; // If we dont have any files in this dir - return
    }

    int run() {
        std::string directory;
        std::string filename;
        // User inputs
        std::cout << "Enter a directory:" << std::endl;
        getline(std::cin, directory);
        std::cout << "Enter a filename or text to find:" << std::endl;
        getline(std::cin, filename);
        // Search
        std::cout << searchForFile(filename, directory) << std::endl;
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
