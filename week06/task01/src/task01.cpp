#include <iostream>
#include <map>
#include <string>


class Application {
public:
    std::map<char, int> mostCommon(std::string userInput) {
        std::map<char, int> amounts;
        for (int i; i < userInput.size(); i++) {
            amounts[userInput[i]] += 1;
        }
        char mostCommonChar;
        int max = 0;
        for (auto i = amounts.begin(); i != amounts.end(); ++i) {
            if (i->second > max) {
                max = i->second;
                mostCommonChar = i->first;
            }
        }
        std::map<char,int> result{{mostCommonChar, max}};
        return result;
    }

    int run() {
        std::string userInput;
        std::cout << "Enter a string:" << std::endl;
        getline(std::cin, userInput);
        std::map<char, int> mostCommonChar = mostCommon(userInput);
        for (auto i = mostCommonChar.begin(); i != mostCommonChar.end(); ++i) {
            std::cout << "['" << i->first
            << "', " << i->second << "]" << std::endl;
        }
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
