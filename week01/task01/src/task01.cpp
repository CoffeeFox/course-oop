#include <iostream>
#include <string>


class Application {
public:
    int run() {
        setlocale(LC_ALL, "Russian");
        std::string username;
        std::cout << "Введите имя пользователя: ";
        std::cin >> username;
        std::cout << "Привет, " << username << "!" << std::endl;
        return 0;
    }
};

int main() {
    Application app;
    return app.run();
}
