#include <iostream>
#include <cmath>


class Application {
public:
    int run() {
        int lenght;
        std::cout << "Enter icosahedron's edge lenght: ";
        std::cin >> lenght;

        double volume = 5/12.0 * (3 + sqrt(5)) * pow(lenght, 3);
        std::cout << "Volume = " << volume << std::endl;
        return 0;
    }
};

int main() {
    Application app;
    return app.run();
}
