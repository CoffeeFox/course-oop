#include <iostream>
#include <limits>


class Application {
public:
    int run() {
        std::cout << "Type\t|" << "Min\t\t\t|" << "Max\n"
        << "Int\t|" << std::numeric_limits<int>::min() << "\t\t|"
        << std::numeric_limits<int>::max() << "\n"; 

        std::cout
        << "Double\t|" << std::numeric_limits<double>::min() << "\t\t|"
        << std::numeric_limits<double>::max() << "\n"; 

        std::cout
        << "Bool\t|" << std::numeric_limits<bool>::min() << "\t\t\t|"
        << std::numeric_limits<bool>::max() << "\n"; 

        std::cout
        << "Short\t|" << std::numeric_limits<short>::min() << "\t\t\t|"
        << std::numeric_limits<short>::max() << "\n"; 

        std::cout
        << "Long\t|" << std::numeric_limits<long>::min() << "\t|"
        << std::numeric_limits<long>::max() << "\n"; 

        std::cout
        << "Float\t|" << std::numeric_limits<float>::min() << "\t\t|"
        << std::numeric_limits<float>::max() << "\n"; 
        return 0;
    }
};


int main() {
    Application app;
    return app.run();
}
